/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"about": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// script path function
/******/ 	function jsonpScriptSrc(chunkId) {
/******/ 		return __webpack_require__.p + "js/" + ({}[chunkId]||chunkId) + ".js"
/******/ 	}
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		var promises = [];
/******/
/******/
/******/ 		// JSONP chunk loading for javascript
/******/
/******/ 		var installedChunkData = installedChunks[chunkId];
/******/ 		if(installedChunkData !== 0) { // 0 means "already installed".
/******/
/******/ 			// a Promise means "currently loading".
/******/ 			if(installedChunkData) {
/******/ 				promises.push(installedChunkData[2]);
/******/ 			} else {
/******/ 				// setup Promise in chunk cache
/******/ 				var promise = new Promise(function(resolve, reject) {
/******/ 					installedChunkData = installedChunks[chunkId] = [resolve, reject];
/******/ 				});
/******/ 				promises.push(installedChunkData[2] = promise);
/******/
/******/ 				// start chunk loading
/******/ 				var script = document.createElement('script');
/******/ 				var onScriptComplete;
/******/
/******/ 				script.charset = 'utf-8';
/******/ 				script.timeout = 120;
/******/ 				if (__webpack_require__.nc) {
/******/ 					script.setAttribute("nonce", __webpack_require__.nc);
/******/ 				}
/******/ 				script.src = jsonpScriptSrc(chunkId);
/******/
/******/ 				// create error before stack unwound to get useful stacktrace later
/******/ 				var error = new Error();
/******/ 				onScriptComplete = function (event) {
/******/ 					// avoid mem leaks in IE.
/******/ 					script.onerror = script.onload = null;
/******/ 					clearTimeout(timeout);
/******/ 					var chunk = installedChunks[chunkId];
/******/ 					if(chunk !== 0) {
/******/ 						if(chunk) {
/******/ 							var errorType = event && (event.type === 'load' ? 'missing' : event.type);
/******/ 							var realSrc = event && event.target && event.target.src;
/******/ 							error.message = 'Loading chunk ' + chunkId + ' failed.\n(' + errorType + ': ' + realSrc + ')';
/******/ 							error.name = 'ChunkLoadError';
/******/ 							error.type = errorType;
/******/ 							error.request = realSrc;
/******/ 							chunk[1](error);
/******/ 						}
/******/ 						installedChunks[chunkId] = undefined;
/******/ 					}
/******/ 				};
/******/ 				var timeout = setTimeout(function(){
/******/ 					onScriptComplete({ type: 'timeout', target: script });
/******/ 				}, 120000);
/******/ 				script.onerror = script.onload = onScriptComplete;
/******/ 				document.head.appendChild(script);
/******/ 			}
/******/ 		}
/******/ 		return Promise.all(promises);
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// on error function for async loading
/******/ 	__webpack_require__.oe = function(err) { console.error(err); throw err; };
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([0,"chunk-vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/cache-loader/dist/cjs.js?!./node_modules/babel-loader/lib/index.js!./node_modules/cache-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./src/module/about/App.vue?vue&type=script&lang=js":
/*!*************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader/lib??vue-loader-options!./src/module/about/App.vue?vue&type=script&lang=js ***!
  \*************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  name: 'App',\n  title: '',\n  provide() {\n    return {\n      reload: this.reload\n    };\n  },\n  data() {\n    return {\n      // 翻页动画\n      transitionName: '',\n      // 页面缓存\n      isRouterAlive: true\n    };\n  },\n  methods: {\n    // 组件刷新\n    reload() {\n      let vue = this;\n      vue.isRouterAlive = false;\n      // 在修改数据之后使用 $nextTick，则可以在回调中获取更新后的 DOM\n      vue.$nextTick(() => {\n        vue.isRouterAlive = true;\n      });\n    }\n  },\n  watch: {\n    $route(to, from) {\n      const vue = this;\n      // 页面切换动画\n      if (to.meta.index > from.meta.index) {\n        vue.transitionName = 'slide-left';\n      } else {\n        vue.transitionName = 'slide-right';\n      }\n    }\n  }\n});\n\n//# sourceURL=webpack:///./src/module/about/App.vue?./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader/lib??vue-loader-options");

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"9e1431c2-vue-loader-template\"}!./node_modules/cache-loader/dist/cjs.js?!./node_modules/babel-loader/lib/index.js!./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./src/module/about/App.vue?vue&type=template&id=023e8275":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"9e1431c2-vue-loader-template"}!./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader/lib??vue-loader-options!./src/module/about/App.vue?vue&type=template&id=023e8275 ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"render\", function() { return render; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"staticRenderFns\", function() { return staticRenderFns; });\nvar render = function render() {\n  var _vm = this,\n    _c = _vm._self._c;\n  return _c(\"div\", {\n    attrs: {\n      id: \"app\"\n    }\n  }, [_c(\"transition\", {\n    attrs: {\n      name: _vm.transitionName,\n      appear: \"\",\n      tag: \"section\"\n    }\n  }, [_c(\"router-view\")], 1)], 1);\n};\nvar staticRenderFns = [];\nrender._withStripped = true;\n\n\n//# sourceURL=webpack:///./src/module/about/App.vue?./node_modules/cache-loader/dist/cjs.js?%7B%22cacheDirectory%22:%22node_modules/.cache/vue-loader%22,%22cacheIdentifier%22:%229e1431c2-vue-loader-template%22%7D!./node_modules/cache-loader/dist/cjs.js??ref--13-0!./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader/lib??vue-loader-options");

/***/ }),

/***/ "./src/module/about/App.vue":
/*!**********************************!*\
  !*** ./src/module/about/App.vue ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _App_vue_vue_type_template_id_023e8275__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./App.vue?vue&type=template&id=023e8275 */ \"./src/module/about/App.vue?vue&type=template&id=023e8275\");\n/* harmony import */ var _App_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./App.vue?vue&type=script&lang=js */ \"./src/module/about/App.vue?vue&type=script&lang=js\");\n/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ \"./node_modules/vue-loader/lib/runtime/componentNormalizer.js\");\n\n\n\n\n\n/* normalize component */\n\nvar component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(\n  _App_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"],\n  _App_vue_vue_type_template_id_023e8275__WEBPACK_IMPORTED_MODULE_0__[\"render\"],\n  _App_vue_vue_type_template_id_023e8275__WEBPACK_IMPORTED_MODULE_0__[\"staticRenderFns\"],\n  false,\n  null,\n  null,\n  null\n  \n)\n\n/* hot reload */\nif (false) { var api; }\ncomponent.options.__file = \"src/module/about/App.vue\"\n/* harmony default export */ __webpack_exports__[\"default\"] = (component.exports);\n\n//# sourceURL=webpack:///./src/module/about/App.vue?");

/***/ }),

/***/ "./src/module/about/App.vue?vue&type=script&lang=js":
/*!**********************************************************!*\
  !*** ./src/module/about/App.vue?vue&type=script&lang=js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_13_0_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/cache-loader/dist/cjs.js??ref--13-0!../../../node_modules/babel-loader/lib!../../../node_modules/cache-loader/dist/cjs.js??ref--1-0!../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=script&lang=js */ \"./node_modules/cache-loader/dist/cjs.js?!./node_modules/babel-loader/lib/index.js!./node_modules/cache-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./src/module/about/App.vue?vue&type=script&lang=js\");\n/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__[\"default\"] = (_node_modules_cache_loader_dist_cjs_js_ref_13_0_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"]); \n\n//# sourceURL=webpack:///./src/module/about/App.vue?");

/***/ }),

/***/ "./src/module/about/App.vue?vue&type=template&id=023e8275":
/*!****************************************************************!*\
  !*** ./src/module/about/App.vue?vue&type=template&id=023e8275 ***!
  \****************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_9e1431c2_vue_loader_template_node_modules_cache_loader_dist_cjs_js_ref_13_0_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_loaders_templateLoader_js_ref_6_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_023e8275__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"9e1431c2-vue-loader-template\"}!../../../node_modules/cache-loader/dist/cjs.js??ref--13-0!../../../node_modules/babel-loader/lib!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ref--6!../../../node_modules/cache-loader/dist/cjs.js??ref--1-0!../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=template&id=023e8275 */ \"./node_modules/cache-loader/dist/cjs.js?{\\\"cacheDirectory\\\":\\\"node_modules/.cache/vue-loader\\\",\\\"cacheIdentifier\\\":\\\"9e1431c2-vue-loader-template\\\"}!./node_modules/cache-loader/dist/cjs.js?!./node_modules/babel-loader/lib/index.js!./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./src/module/about/App.vue?vue&type=template&id=023e8275\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"render\", function() { return _node_modules_cache_loader_dist_cjs_js_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_9e1431c2_vue_loader_template_node_modules_cache_loader_dist_cjs_js_ref_13_0_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_loaders_templateLoader_js_ref_6_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_023e8275__WEBPACK_IMPORTED_MODULE_0__[\"render\"]; });\n\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"staticRenderFns\", function() { return _node_modules_cache_loader_dist_cjs_js_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_9e1431c2_vue_loader_template_node_modules_cache_loader_dist_cjs_js_ref_13_0_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_loaders_templateLoader_js_ref_6_node_modules_cache_loader_dist_cjs_js_ref_1_0_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_023e8275__WEBPACK_IMPORTED_MODULE_0__[\"staticRenderFns\"]; });\n\n\n\n//# sourceURL=webpack:///./src/module/about/App.vue?");

/***/ }),

/***/ "./src/module/about/main.js":
/*!**********************************!*\
  !*** ./src/module/about/main.js ***!
  \**********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ \"./node_modules/vue/dist/vue.runtime.esm.js\");\n/* harmony import */ var _App_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./App.vue */ \"./src/module/about/App.vue\");\n/* harmony import */ var _router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/router */ \"./src/router/index.js\");\n/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/store */ \"./src/store/index.js\");\n\n\n\n\nvue__WEBPACK_IMPORTED_MODULE_0__[\"default\"].config.productionTip = false;\nconst log = Object({\"NODE_ENV\":\"dev\",\"VUE_APP_BASE\":\"dev\",\"VUE_APP_BASE_URL\":\"/api\",\"VUE_APP_BRANCH\":\"\",\"VUE_APP_DOMAIN_NAME\":\"\",\"VUE_APP_SERVER_NAME\":\"\",\"BASE_URL\":\"/\"});\nconsole.log(`%c 当前系统环境 %c 【${log.NODE_ENV}】 %c`, 'background:rgba(0,0,0,.5) ; padding: 5px; border-radius: 3px 0 0 3px;  color: #fff', 'background: rgba(0,0,0,1) ; padding: 5px 50px; border-radius: 0 3px 3px 0;  color: #fff', 'background:transparent');\nconsole.log(log);\nnew vue__WEBPACK_IMPORTED_MODULE_0__[\"default\"]({\n  router: _router__WEBPACK_IMPORTED_MODULE_2__[\"default\"],\n  store: _store__WEBPACK_IMPORTED_MODULE_3__[\"default\"],\n  render: h => h(_App_vue__WEBPACK_IMPORTED_MODULE_1__[\"default\"])\n}).$mount('#app');\n\n//# sourceURL=webpack:///./src/module/about/main.js?");

/***/ }),

/***/ "./src/router/about/index.js":
/*!***********************************!*\
  !*** ./src/router/about/index.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _lazyRouter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../lazyRouter */ \"./src/router/lazyRouter.js\");\n\n\n\nconst router = [{\n  path: '/about',\n  redirect: {\n    name: 'news'\n  }\n}, {\n  path: '/about/news',\n  name: 'news',\n  meta: {\n    title: '新闻',\n    index: 0,\n    keepAlive: false,\n    requireAuth: false,\n    scale: true\n  },\n  component: Object(_lazyRouter__WEBPACK_IMPORTED_MODULE_0__[\"aboutViewPath\"])('news/index')\n}];\n/* harmony default export */ __webpack_exports__[\"default\"] = (router);\n\n//# sourceURL=webpack:///./src/router/about/index.js?");

/***/ }),

/***/ "./src/router/home/index.js":
/*!**********************************!*\
  !*** ./src/router/home/index.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _lazyRouter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../lazyRouter */ \"./src/router/lazyRouter.js\");\n\n\n\nconst router = [{\n  path: '/home',\n  redirect: {\n    name: 'news'\n  }\n}, {\n  path: '/home/news',\n  name: 'news',\n  meta: {\n    title: '新闻',\n    index: 0,\n    keepAlive: false,\n    requireAuth: false,\n    scale: true\n  },\n  component: Object(_lazyRouter__WEBPACK_IMPORTED_MODULE_0__[\"homeViewPath\"])('news/index')\n}];\n/* harmony default export */ __webpack_exports__[\"default\"] = (router);\n\n//# sourceURL=webpack:///./src/router/home/index.js?");

/***/ }),

/***/ "./src/router/index.js":
/*!*****************************!*\
  !*** ./src/router/index.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* WEBPACK VAR INJECTION */(function(__dirname) {/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ \"./node_modules/vue/dist/vue.runtime.esm.js\");\n/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-router */ \"./node_modules/vue-router/dist/vue-router.esm.js\");\n/* harmony import */ var _home_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home/index */ \"./src/router/home/index.js\");\n/* harmony import */ var _about_index__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./about/index */ \"./src/router/about/index.js\");\n/* harmony import */ var _utils_SessionStorage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/utils/SessionStorage */ \"./src/utils/SessionStorage.js\");\n\n\n\n\n\n\n\nvue__WEBPACK_IMPORTED_MODULE_0__[\"default\"].use(vue_router__WEBPACK_IMPORTED_MODULE_1__[\"default\"]);\n/**\r\n * @type {[{path: string, component: (function(): Promise<{readonly default?: {name: string}}>), meta: {keepAlive: boolean, requireAuth: boolean, title: string}, name: string}, {redirect: {name: string}, path: string}, {path: string, component: function(*): void, meta: {keepAlive: boolean, requireAuth: boolean, index: number, scale: boolean, title: string}, name: string}, {path: string, component: function(*): void, meta: {keepAlive: boolean, requireAuth: boolean, index: number, title: string}, name: string}, {path: string, component: function(*): void, children: [{path: string, component: function(*): void, meta: {keepAlive: boolean, requireAuth: boolean, index: number, title: string}, name: string}]}]}\r\n */\nconst routes = [\n// {\n//   path: '*',\n//   name: '404',\n//   meta: { title: '404', keepAlive: true, requireAuth: false },\n//   component: () => import('@/module/error/404.vue')\n// },\n..._home_index__WEBPACK_IMPORTED_MODULE_2__[\"default\"], ..._about_index__WEBPACK_IMPORTED_MODULE_3__[\"default\"]];\n/**\r\n * 获取原型对象上的push函数\r\n * @type {{(location: RawLocation): Promise<Route>, (location: RawLocation, onComplete?: Function, onAbort?: ErrorHandler): void}}\r\n */\nconst originalPush = vue_router__WEBPACK_IMPORTED_MODULE_1__[\"default\"].prototype.push;\n/**\r\n * 修改原型对象中的push方法\r\n * @param location\r\n * @returns {Promise<Route>}\r\n */\nvue_router__WEBPACK_IMPORTED_MODULE_1__[\"default\"].prototype.push = function push(location) {\n  return originalPush.call(this, location).catch(err => err);\n};\n/**\r\n * 配置滚动条的位置\r\n * @param to\r\n * @param from\r\n * @param savedPosition\r\n * @returns {{x: number, y: number}|*}\r\n */\nconst scrollBehavior = (to, from, savedPosition) => {\n  if (savedPosition) {\n    return savedPosition;\n  } else {\n    return {\n      x: 0,\n      y: 0\n    };\n  }\n};\n/**\r\n * 路由配置\r\n * @type {VueRouter}\r\n */\nconst router = new vue_router__WEBPACK_IMPORTED_MODULE_1__[\"default\"]({\n  base: __dirname,\n  likActiveClass: 'link-active',\n  scrollBehavior,\n  routes\n});\n/**\r\n *  修改网站title的值\r\n */\nrouter.afterEach(transition => {\n  if (transition.meta.title) {\n    document.title = transition.meta.title;\n  }\n});\n/**\r\n * 路由拦截器\r\n * 1.判断该路由是否需要登录权限\r\n * 2.通过vuex state获取当前的token是否存在\r\n * 3.将跳转的路由path作为参数，登录成功后跳转到该路由\r\n */\nrouter.beforeEach((to, from, next) => {\n  const userInfo = _utils_SessionStorage__WEBPACK_IMPORTED_MODULE_4__[\"default\"].GET('loginInfo');\n  if (to.meta.requireAuth) {\n    if (userInfo && userInfo.token) {\n      next();\n    } else {\n      next({\n        path: '/login',\n        query: {\n          redirectUrl: to.fullPath\n        }\n      });\n    }\n  } else {\n    next();\n  }\n});\n/* harmony default export */ __webpack_exports__[\"default\"] = (router);\n/* WEBPACK VAR INJECTION */}.call(this, \"/\"))\n\n//# sourceURL=webpack:///./src/router/index.js?");

/***/ }),

/***/ "./src/router/lazyRouter.js":
/*!**********************************!*\
  !*** ./src/router/lazyRouter.js ***!
  \**********************************/
/*! exports provided: homeViewPath, aboutViewPath, viewPath */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"homeViewPath\", function() { return homeViewPath; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"aboutViewPath\", function() { return aboutViewPath; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"viewPath\", function() { return viewPath; });\n\n\n/**\r\n * home\r\n * @param path\r\n * @returns {(function(*): void)|*}\r\n */\nfunction homeViewPath(path) {\n  return viewPath('home/pages/' + path);\n}\n\n/**\r\n * about\r\n * @param path\r\n * @returns {(function(*): void)|*}\r\n */\nfunction aboutViewPath(path) {\n  return viewPath('about/pages/' + path);\n}\nfunction viewPath(path) {\n  return resolve => {\n    __webpack_require__.e(/*! AMD require */ 0).then(function() { var __WEBPACK_AMD_REQUIRE_ARRAY__ = [__webpack_require__(\"./src/module sync recursive ^\\\\.\\\\/.*\\\\.vue$\")(\"./\" + path + \".vue\")]; (component => {\n      resolve(component);\n    }).apply(null, __WEBPACK_AMD_REQUIRE_ARRAY__);}).catch(__webpack_require__.oe);\n  };\n}\n\n//# sourceURL=webpack:///./src/router/lazyRouter.js?");

/***/ }),

/***/ "./src/store/index.js":
/*!****************************!*\
  !*** ./src/store/index.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ \"./node_modules/vue/dist/vue.runtime.esm.js\");\n/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ \"./node_modules/vuex/dist/vuex.esm.js\");\n/* harmony import */ var _store_modules_home__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/store/modules/home */ \"./src/store/modules/home.js\");\n/* harmony import */ var _store_modules_about__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/store/modules/about */ \"./src/store/modules/about.js\");\n\n\n\n\nvue__WEBPACK_IMPORTED_MODULE_0__[\"default\"].use(vuex__WEBPACK_IMPORTED_MODULE_1__[\"default\"]);\n/* harmony default export */ __webpack_exports__[\"default\"] = (new vuex__WEBPACK_IMPORTED_MODULE_1__[\"default\"].Store({\n  modules: {\n    homeStore: _store_modules_home__WEBPACK_IMPORTED_MODULE_2__[\"default\"],\n    aboutStore: _store_modules_about__WEBPACK_IMPORTED_MODULE_3__[\"default\"]\n  }\n}));\n\n//# sourceURL=webpack:///./src/store/index.js?");

/***/ }),

/***/ "./src/store/modules/about.js":
/*!************************************!*\
  !*** ./src/store/modules/about.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/**\r\n * about\r\n */\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  state: {},\n  mutations: {}\n});\n\n//# sourceURL=webpack:///./src/store/modules/about.js?");

/***/ }),

/***/ "./src/store/modules/home.js":
/*!***********************************!*\
  !*** ./src/store/modules/home.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/**\r\n * 首页\r\n */\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  state: {},\n  mutations: {}\n});\n\n//# sourceURL=webpack:///./src/store/modules/home.js?");

/***/ }),

/***/ "./src/utils/SessionStorage.js":
/*!*************************************!*\
  !*** ./src/utils/SessionStorage.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  /**\r\n   * @description 保存SessionStorage\r\n   * @param key {string}  需要存储SessionStorage的key\r\n   * @param value {{type, token: *}}  需要存储SessionStorage的value\r\n   */\n  SET(key = 'TEST_STORAGE', value = {} || '') {\n    window.sessionStorage.setItem(key, typeof value === 'string' ? value : JSON.stringify(value));\n  },\n  /**\r\n   * @description 获取SessionStorage\r\n   * @param key {String}  需要获取SessionStorage的key\r\n   * @returns {string|null} 返回SessionStorage的内容\r\n   */\n  GET(key = 'TEST_STORAGE') {\n    const value = sessionStorage.getItem(key);\n    return typeof value === 'string' ? JSON.parse(value) : value;\n  },\n  /**\r\n   * @description 删除SessionStorage\r\n   * @param key {String}  需要删除SessionStorage的key\r\n   */\n  DEL(key = 'TEST_STORAGE') {\n    sessionStorage.removeItem(key);\n  },\n  /**\r\n   * @description 保存SessionStorage集合（示例：GroupSet ('SessionStorage_test', 'time', '2020-12-12')）\r\n   * @param groupKey {String} 需要存储SessionStorage集合key(最外层的key)\r\n   * @param key {String} 需要存储SessionStorage的key\r\n   * @param value 需要存储SessionStorage集合中的value\r\n   */\n  GroupSet(groupKey, key, value) {\n    let obj = this.GET(groupKey) || {};\n    if (obj) {\n      obj[key] = value;\n    }\n    this.SET(groupKey, obj);\n  },\n  /**\r\n   * @description 获取SessionStorage集合中子类\r\n   * @param groupKey {String} 需要获取SessionStorage集合key(最外层的key)\r\n   * @param key {String} 需要获取SessionStorage集合中某个key\r\n   * @param value 需要获取SessionStorage集合中某个key对应的value\r\n   */\n  GroupGet(groupKey, key) {\n    const obj = this.GET(groupKey);\n    return obj && obj[key];\n  },\n  /**\r\n   * @description 删除SessionStorage集合中子类\r\n   * @param key {String}  需要删除SessionStorage集合子类的key\r\n   */\n  GroupDel(groupKey, key) {\n    const obj = this.GET(groupKey);\n    if (obj && obj[key]) {\n      delete obj[key];\n      this.SET(groupKey, obj);\n    }\n  }\n});\n\n//# sourceURL=webpack:///./src/utils/SessionStorage.js?");

/***/ }),

/***/ 0:
/*!****************************************!*\
  !*** multi ./src/module/about/main.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = __webpack_require__(/*! E:\\Gitee\\vue-single-page\\src\\module\\about\\main.js */\"./src/module/about/main.js\");\n\n\n//# sourceURL=webpack:///multi_./src/module/about/main.js?");

/***/ })

/******/ });