#### 项目模板 v1.0.0

---
#### 项目描述
```
适用于大屏页面的项目模板，或其他VUE项目的基线【多页面，多入口】(例如：项目中包含大屏和多个后台管理系统)

在模块（module）文件夹下，新建文件夹（必须包含,App.vue,main.js）
在路由（router）文件夹下，新建路由，并在index.js中引入
```
#### 开发团队
```
产品经理: ***
项目经理: ***
研发经理: ***
设计开发: ***
前端开发: ***
后端开发: ***
运维管理: ***
```

#### 截图展示
![](home.png)
#### 下载依赖
```
npm install
yarn install
```
#### 运行
```
npm run serve
yarn serve
```
#### 打包开发环境
```
npm run build-dev
yarn build-dev
```
#### 打包测试环境
```
npm run build-test
yarn build-test
```
#### 打包生产环境
```
npm run build-prod
yarn build-prod
```
#### 直接生成生产环境的zip压缩包
```
npm run deploy
yarn build-deploy
```

#### 目录结构：
```
├── public                     // public
├── src                        // 源代码
│   ├── api                    // 接口
│   ├── assets                 // 静态资源
│   ├── common                 // 通用方法
│   ├── components             // 全局通用组件
│   ├── dictionary             // 全局通用数据字典
│   ├── directives             // 全局通用自定义指令
│   ├── filters                // 全局通用过滤器
│   ├── layout                 // 布局(详见layout/readme.md)
│   ├── router                 // 路由(详见router/readme.md)
│   ├── store                  // 状态存储(详见store/readme.md)
│   ├── utils                  // 工具类(详见utils/readme.md)
│   ├── module                 // 模块【页面在此】(详见modulereadme.md)
│   └── setting.jsc            // 项目设置
├── compress.js                // 压缩脚本   
├── alias.config.js            // 别名配置(详见文件注释)
└── vue.config.js              // vue-cli 配置
```
