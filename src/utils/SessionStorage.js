'use strict'
export default {
  /**
   * @description 保存SessionStorage
   * @param key {string}  需要存储SessionStorage的key
   * @param value {{type, token: *}}  需要存储SessionStorage的value
   */
  SET (key = 'TEST_STORAGE', value = {} || '') {
    window.sessionStorage.setItem(
      key,
      typeof value === 'string' ? value : JSON.stringify(value)
    )
  },
  /**
   * @description 获取SessionStorage
   * @param key {String}  需要获取SessionStorage的key
   * @returns {string|null} 返回SessionStorage的内容
   */
  GET (key = 'TEST_STORAGE') {
    const value = sessionStorage.getItem(key)
    return typeof value === 'string' ? JSON.parse(value) : value
  },
  /**
   * @description 删除SessionStorage
   * @param key {String}  需要删除SessionStorage的key
   */
  DEL (key = 'TEST_STORAGE') {
    sessionStorage.removeItem(key)
  },
  /**
   * @description 保存SessionStorage集合（示例：GroupSet ('SessionStorage_test', 'time', '2020-12-12')）
   * @param groupKey {String} 需要存储SessionStorage集合key(最外层的key)
   * @param key {String} 需要存储SessionStorage的key
   * @param value 需要存储SessionStorage集合中的value
   */
  GroupSet (groupKey, key, value) {
    let obj = this.GET(groupKey) || {}
    if (obj) {
      obj[key] = value
    }
    this.SET(groupKey, obj)
  },
  /**
   * @description 获取SessionStorage集合中子类
   * @param groupKey {String} 需要获取SessionStorage集合key(最外层的key)
   * @param key {String} 需要获取SessionStorage集合中某个key
   * @param value 需要获取SessionStorage集合中某个key对应的value
   */
  GroupGet (groupKey, key) {
    const obj = this.GET(groupKey)
    return obj && obj[key]
  },
  /**
   * @description 删除SessionStorage集合中子类
   * @param key {String}  需要删除SessionStorage集合子类的key
   */
  GroupDel (groupKey, key) {
    const obj = this.GET(groupKey)
    if (obj && obj[key]) {
      delete obj[key]
      this.SET(groupKey, obj)
    }
  }
}
