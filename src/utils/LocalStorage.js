'use strict'
export default {
  /**
   * @description 保存localStorage
   * @param key {string}  需要存储localStorage的key
   * @param value {string}  需要存储localStorage的value
   */
  SET (key = 'TEST_STORAGE', value = {} || '') {
    window.localStorage.setItem(
      key,
      value instanceof String ? value : JSON.stringify(value)
    )
  },
  /**
   * @description 获取localStorage
   * @param key {String}  需要获取localStorage的key
   * @returns {string|null} 返回localStorage的内容
   */
  GET (key = 'TEST_STORAGE') {
    const value = localStorage.getItem(key)
    return value instanceof String ? JSON.parse(value) : value
  },
  /**
   * @description 删除localStorage
   * @param key {String}  需要删除localStorage的key
   */
  Del (key = 'TEST_STORAGE') {
    localStorage.removeItem(key)
  },
  /**
   * @description 保存localStorage集合（示例：GroupSet ('localStorage_test', 'time', '2020-12-12')）
   * @param groupKey {String} 需要存储localStorage集合key(最外层的key)
   * @param key {String} 需要存储localStorage的key
   * @param value 需要存储localStorage集合中的value
   */
  GroupSet (groupKey, key, value) {
    let obj = this.GET(groupKey) || {}
    if (obj) {
      obj[key] = value
    }
    this.SET(groupKey, obj)
  },
  /**
   * @description 获取localStorage集合中子类
   * @param groupKey {String} 需要获取localStorage集合key(最外层的key)
   * @param key {String} 需要获取localStorage集合中某个key
   */
  GroupGet (groupKey, key) {
    const obj = this.GET(groupKey)
    return obj && obj[key]
  },
  /**
   * @description 删除localStorage集合中子类
   * @param groupKey
   * @param key {String}  需要删除localStorage集合子类的key
   */
  GroupDel (groupKey, key) {
    const obj = this.GET(groupKey)
    if (obj && obj[key]) {
      delete obj[key]
      this.SET(groupKey, obj)
    }
  }
}
