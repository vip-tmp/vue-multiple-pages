#### 工具类说明：
```
├── AutoScale.js                // 页面自动缩放   
├── CurrentTime.js              // 当前时间日期星期
└── LocalStorage.js             // LocalStorage
└── SessionStorage.js           // SessionStorage
└── Request.js                  // 请求库
└── Validate.js                 // 验证库
```
