import Vue from 'vue'
import Vuex from 'vuex'
import homeStore from '@/store/modules/home'
import aboutStore from '@/store/modules/about'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    homeStore,
    aboutStore
  }
})
