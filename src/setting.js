'use strict'
/**
 * @description 项目设置
 * @param title {string}          (VUE)     网页标题名称
 * @param  version {string}       (1.0.0)   版本号
 * @param  timestamp {number}    (160935686614) 时间戳
 * @author
 */
module.exports = {
  title: '通用大屏项目模板',
  version: '1.0.0',
  timestamp: new Date().getTime()
}
