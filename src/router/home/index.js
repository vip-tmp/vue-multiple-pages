'use strict'
import { homeViewPath } from '../lazyRouter'
const router = [
  {
    path: '/home',
    redirect: { name: 'news' }
  },
  {
    path: '/home/news',
    name: 'news',
    meta: { title: '新闻', index: 0, keepAlive: false, requireAuth: false, scale: true },
    component: homeViewPath('news/index')
  }
]
export default router
