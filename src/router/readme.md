#### 目录和参数说明

---
```
├── router                     // 路由信息
│   ├── lazyRouter.js          // 路由懒加载配置
│   ├── page.js                // 页面路由[*]
└── └── index.js               // 路由入口文件
├── home                       // 首页
│   ├── index.js               // 路由列表
├── about                      // 路由信息
└── └── index.js               // 路由列表
```

#### 路由参数说明

---
```
/**
 * @description 添加页面路由
 * @param path {string} 页面的路径
 * @param name {string}  页面路径别名
 * @param meta {string}  页面的说明（详见meta参数详解）
 * @param component {string} 组件的路径
 * @param children { {} } 组件的路径
 * @param redirect {string}  页面重定向
 */

/**
 * @description meta参数详解
 * @param title {string} 浏览器页面标题
 * @param index {number} 页面出场顺序动画
 * @param keepAlive {boolean} 页面是否缓存（默认false）
 * @param requireAuth {boolean} 页面是否需要登录（默认false）
 * @param scale {number} [1: 全屏自适应（两边不留白）,   2：全屏居中自适应（两边留白）,  3：滚动条（默认）]
 */
```
