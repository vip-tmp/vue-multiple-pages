'use strict'

/**
 * home
 * @param path
 * @returns {(function(*): void)|*}
 */
export function homeViewPath (path) {
  return viewPath('home/pages/' + path)
}

/**
 * about
 * @param path
 * @returns {(function(*): void)|*}
 */
export function aboutViewPath (path) {
  return viewPath('about/pages/' + path)
}
export function viewPath (path) {
  return resolve => {
    require(['@/module/' + path + '.vue'], component => {
      resolve(component)
    })
  }
}
