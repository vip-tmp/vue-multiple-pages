'use strict'
import { aboutViewPath } from '../lazyRouter'
const router = [
  {
    path: '/about',
    redirect: { name: 'news' }
  },
  {
    path: '/about/news',
    name: 'news',
    meta: { title: '新闻', index: 0, keepAlive: false, requireAuth: false, scale: true },
    component: aboutViewPath('news/index')
  }
]
export default router
