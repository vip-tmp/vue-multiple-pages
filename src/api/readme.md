### API封装（【如：user.js】）
~~~

一、方法一

export default {
  login (data) {
    return Request({ url: '/api/login', method: 'post', data })
  },
  smsCode (data) {
    return Request({ url: '/api/smsCode', method: 'get', data })
  }
}

二、方法二

export function login (data) {
  return Request({ url: '/api/login', method: 'post', data })
}
export function smsCode (data) {
  return Request({ url: '/api/smsCode', method: 'get', data })
}
~~~
### API的使用
~~~

一、方法一（推荐）

import api from '@/util/user'
getData () {
  const parameter = {}
  const result = await api.login()
}

二、方法二

import { login, smsCode } from '@/util/user'
getData () {
  const parameter = {}
  const result = await login()
}
~~~
