'use strict'
import { POST, GET } from '@/utils/Request'
export default {
  login (params) {
    return POST('/api/login', params)
  },
  list (params) {
    return GET('/api/list', { params })
  }
}
