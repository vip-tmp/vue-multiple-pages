import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import store from '@/store'
Vue.config.productionTip = false
const log = process.env
console.log(
  `%c 当前系统环境 %c 【${log.NODE_ENV}】 %c`,
  'background:rgba(0,0,0,.5) ; padding: 5px; border-radius: 3px 0 0 3px;  color: #fff',
  'background: rgba(0,0,0,1) ; padding: 5px 50px; border-radius: 0 3px 3px 0;  color: #fff',
  'background:transparent'
)
console.log(log)
new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
